package com.afs.restapi.repository;

import com.afs.restapi.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyJPARepository extends JpaRepository<Company, Long> {
}
