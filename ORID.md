## O:

Learned the concepts and use of JPA.

## R:

I find this feature very useful

## I:

- Learning JPA enables developers to operate on databases in an object-oriented manner without writing cumbersome SQL statements.

- By using @Entity,@Table,@Id,@GeneratedValue annotations in the model layer, you can generate the corresponding database tables directly from the variables that have been created, which greatly simplifies the operations on the database

## D:

JPA provides a mechanism for mapping database table records to Java objects, allowing developers to manipulate the database in an object-oriented manner without writing cumbersome SQL statements.